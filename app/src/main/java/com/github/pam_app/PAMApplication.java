package com.github.pam_app;

import android.app.Application;

import com.github.pam_app.session.SessionKeys;

import hundredthirtythree.sessionmanager.SessionManager;

/**
 * Created by chingizh on 2019-10-16 long live Chingiz!
 */
public class PAMApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        new SessionManager.Builder()
                .setContext(getApplicationContext())
                .setPrefsName(SessionKeys.PREFS_NAME.getKey())
                .build();
    }
}
