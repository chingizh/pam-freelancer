package com.github.pam_app;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import hundredthirtythree.sessionmanager.SessionManager;

import android.util.Log;
import android.view.View;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.ToggleButton;

import com.github.pam_app.alarm.AlarmReceiver;
import com.github.pam_app.session.SessionKeys;

import java.util.Calendar;

public class SetAlarmActivity extends AppCompatActivity {

    AlarmManager alarmManager;
    private PendingIntent pendingIntent;
    private TimePicker alarmTimePicker;
    private ToggleButton alarmToggle;
    private Switch alarmMode;
    private static SetAlarmActivity inst;
    private TextView alarmTextView;
    private int weekday = 0;

    public static SetAlarmActivity instance() {
        return inst;
    }

    @Override
    public void onStart() {
        super.onStart();
        inst = this;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_set_alarm);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        alarmTimePicker = findViewById(R.id.timePicker);
        alarmTextView = findViewById(R.id.alarmText);
        alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
        alarmToggle = findViewById(R.id.alarmToggle);
        alarmMode = findViewById(R.id.alarmMode);
        Bundle bundle = getIntent().getExtras();
        weekday = bundle.getInt("WEEKDAY");

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
    }

    public void onToggleClicked(View view) {
        if (((ToggleButton) view).isChecked()) {
            Log.d("MyActivity", "Alarm On");
            Calendar calendar = Calendar.getInstance();
//            calendar.set(Calendar.DAY_OF_WEEK, weekday);
            calendar.set(Calendar.HOUR_OF_DAY, alarmTimePicker.getCurrentHour());
            calendar.set(Calendar.MINUTE, alarmTimePicker.getCurrentMinute());
            Intent myIntent = new Intent(SetAlarmActivity.this, AlarmReceiver.class);
            myIntent.putExtra("MODE", alarmMode.isChecked());
            pendingIntent = PendingIntent.getBroadcast(SetAlarmActivity.this, 0, myIntent, 0);
            alarmManager.set(AlarmManager.RTC, calendar.getTimeInMillis(), pendingIntent);
            SessionManager.putLong(SessionKeys.ALARM.getKey(), calendar.getTimeInMillis());
        } else {
            alarmManager.cancel(pendingIntent);
            Log.d("MyActivity", "Alarm Off");
        }
    }

}
