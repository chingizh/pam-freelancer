package com.github.pam_app.alarm;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;

import androidx.legacy.content.WakefulBroadcastReceiver;

/**
 * Created by chingizh on 2019-10-15 long live Chingiz!
 */
public class AlarmReceiver extends WakefulBroadcastReceiver {

    @Override
    public void onReceive(final Context context, Intent intent) {
        //this will update the UI with message
        boolean alarmMode = intent.getExtras().getBoolean("MODE");
        Intent i = new Intent();
        i.setClassName("com.github.pam_app", alarmMode ? "com.github.pam_app.alarm.result.Activity1" : "com.github.pam_app.alarm.result.Activity2");
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(i);

        ComponentName comp = new ComponentName(context.getPackageName(),
                AlarmService.class.getName());
        startWakefulService(context, (intent.setComponent(comp)));
        setResultCode(Activity.RESULT_OK);
    }
}
