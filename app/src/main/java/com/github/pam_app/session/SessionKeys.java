package com.github.pam_app.session;

/**
 * Created by chingizh on 2019-10-16 long live Chingiz!
 */
public enum SessionKeys {
    PREFS_NAME("pam"),
    ALARM("alarm");

    private String key;

    SessionKeys(String key) {
        this.key = key;
    }

    public String getKey() {
        return key;
    }
}
