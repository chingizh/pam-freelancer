package com.github.pam_app.ui.alarm_morning;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.github.pam_app.R;
import com.github.pam_app.SetAlarmActivity;

import java.lang.reflect.Array;

public class MorningFragment extends Fragment {

    private MorningViewModel morningViewModel;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        morningViewModel =
                ViewModelProviders.of(this).get(MorningViewModel.class);
        View root = inflater.inflate(R.layout.fragment_morning, container, false);
        final ListView listView = root.findViewById(R.id.listView);
        morningViewModel.getmWeekdays().observe(this, new Observer<String[]>() {
            @Override
            public void onChanged(String[] strings) {
                ArrayAdapter arrayAdapter = new ArrayAdapter(getContext(), android.R.layout.simple_list_item_1, android.R.id.text1, strings);
                listView.setAdapter(arrayAdapter);
                listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                        Intent intent = new Intent(getContext(), SetAlarmActivity.class);
                        intent.putExtra("WEEKDAY", i+1);
                        startActivity(intent);
                    }
                });
            }
        });
        return root;
    }
}