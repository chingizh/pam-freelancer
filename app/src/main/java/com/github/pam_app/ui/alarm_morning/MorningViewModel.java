package com.github.pam_app.ui.alarm_morning;

import android.app.Application;
import android.widget.ArrayAdapter;

import com.github.pam_app.MainActivity;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class MorningViewModel extends ViewModel {

    private MutableLiveData<String[]> mWeekdays;

    public MorningViewModel() {
        String[] array = {"Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"};
        mWeekdays = new MutableLiveData<>();
        mWeekdays.setValue(array);
    }

    MutableLiveData<String[]> getmWeekdays() {
        return mWeekdays;
    }
}