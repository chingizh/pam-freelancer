package com.github.pam_app.ui.alarm_ram;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TextView;

import com.github.pam_app.R;
import com.github.pam_app.alarm.AlarmReceiver;
import com.github.pam_app.session.SessionKeys;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import hundredthirtythree.sessionmanager.SessionManager;

import static android.content.Context.ALARM_SERVICE;

public class RamFragment extends Fragment {

    private RamViewModel ramViewModel;
    private TextView text;
    private SeekBar interval;
    private Button set;
    AlarmManager alarmManager;
    private PendingIntent pendingIntent;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        ramViewModel =
                ViewModelProviders.of(this).get(RamViewModel.class);
        View root = inflater.inflate(R.layout.fragment_ram, container, false);
        text = root.findViewById(R.id.text_gallery);
        interval = root.findViewById(R.id.seekBar);
        set = root.findViewById(R.id.set);

        alarmManager = (AlarmManager) getContext().getSystemService(ALARM_SERVICE);
        interval.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                text.setText(String.valueOf(i));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
        ramViewModel.getText().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                text.setText(s);
            }
        });

        set.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent myIntent = new Intent(getActivity(), AlarmReceiver.class);
                myIntent.putExtra("MODE", true);
                pendingIntent = PendingIntent.getBroadcast(getActivity(), 0, myIntent, 0);
                long time = SessionManager.getLong(SessionKeys.ALARM.getKey(), 0);
                alarmManager.set(AlarmManager.RTC, time + (interval.getProgress() * 60 * 1000), pendingIntent);
            }
        });

        return root;
    }

}