package com.github.pam_app.ui.alarm_ram;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class RamViewModel extends ViewModel {

    private MutableLiveData<String> mText;

    public RamViewModel() {
        mText = new MutableLiveData<>();
        mText.setValue("Interval");
    }

    public LiveData<String> getText() {
        return mText;
    }
}